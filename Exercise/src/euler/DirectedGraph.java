package euler;

import java.io.*;
import java.util.*;
import java.util.LinkedList;

//1+2 kiểm tra một đồ thị có phải đồ thị euler và nửa euler (đồ thị có hướng)
public class DirectedGraph {
	private int V; // Số đỉnh
	private LinkedList<Integer> adj[];// Danh sách kề
	private int in[];

	// Constructor
	public DirectedGraph(int v) {
		V = v;
		adj = new LinkedList[v];
		in = new int[V];
		for (int i = 0; i < v; ++i) {
			adj[i] = new LinkedList();
			in[i] = 0;
		}
	}

	// Thêm cạnh
	public void addEdge(int v, int w) {
		adj[v].add(w);
		in[w]++;
	}

	public void DFSUtil(int v, Boolean visited[]) {

		visited[v] = true;
		int n;

		// Đánh dấu các đỉnh kề với đỉnh này
		Iterator<Integer> i = adj[v].iterator();
		while (i.hasNext()) {
			n = i.next();
			if (!visited[n])
				DFSUtil(n, visited);
		}
	}

	// Đảo ngược đồ thị
	public DirectedGraph getTranspose() {
		DirectedGraph g = new DirectedGraph(V);
		for (int v = 0; v < V; v++) {
			// Định kỳ cho tất cả các đỉnh kề với đỉnh này
			Iterator<Integer> i = adj[v].listIterator();
			while (i.hasNext()) {
				g.adj[i.next()].add(v);
				(g.in[v])++;
			}
		}
		return g;
	}

	// Hàm chính trả về true nếu đồ thị kết nối mạnh
	public Boolean isSC() {
		// Step 1:Đánh dấu các đỉnh không được thăm (For
		// first DFS)
		Boolean visited[] = new Boolean[V];
		for (int i = 0; i < V; i++)
			visited[i] = false;

		// Step 2: Thực hiện duyệt DFS bắt đầu từ đỉnh đầu tiên.
		DFSUtil(0, visited);

		// Nếu DFS traversal không truy cập tất cả các đỉnh, thì trả về false.
		for (int i = 0; i < V; i++)
			if (visited[i] == false)
				return false;

		// Step 3: Tạo một đồ thị đảo ngược
		DirectedGraph gr = getTranspose();

		// Step 4: Đánh dấu tất cả các đỉnh là không được thăm (Đối với DFS thứ hai)
		for (int i = 0; i < V; i++)
			visited[i] = false;

		// Step 5: Thực hiện DFS cho đồ thị đảo ngược bắt đầu từ đỉnh đầu tiên. Đỉnh bắt
		// đầu phải giống với điểm bắt đầu của DFS đầu tiên
		gr.DFSUtil(0, visited);

		// If all vertices are not visited in second DFS, then
		// return false
		for (int i = 0; i < V; i++)
			if (visited[i] == false)
				return false;

		return true;
	}

	// 1 kiểm tra một đồ thị có phải đồ thị euler

	/*
	 * Hàm này trả về true nếu đồ thị có hướng có chu trình euler, nếu không thì trả
	 * về false
	 */
	public Boolean isEulerianCycle() {
		// Kiểm tra xem tất cả các đỉnh khác 0 có được kết nối với nhau không
		if (isSC() == false)
			return false;

		// Kiểm tra xem mức độ trong và độ ngoài của mọi đỉnh có giống nhau không
		for (int i = 0; i < V; i++)
			if (adj[i].size() != in[i])
				return false;

		return true;
	}

	public static void main(String[] args) throws java.lang.Exception {
		DirectedGraph g = new DirectedGraph(5);
		g.addEdge(1, 0);
		g.addEdge(0, 2);
		g.addEdge(2, 1);
		g.addEdge(0, 3);
		g.addEdge(3, 4);
		g.addEdge(4, 0);

		if (g.isEulerianCycle())
			System.out.println("Đồ thị là Euler");
		else
			System.out.println("Đồ thị Không phải Euler");
	}
}