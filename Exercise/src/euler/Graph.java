package euler;

import java.util.ArrayList;

//3+4 Tìm một chu trình Euler và tìm một hướng đi trong Euler trong đồ thị Euler
//Biểu diễn theo danh sách kề
public class Graph {

	private int vertices; // Số đỉnh
	private ArrayList<Integer>[] adj; // danh sách gần kề

	Graph(int numOfVertices) {
		// khởi tạo số lượng đỉnh
		this.vertices = numOfVertices;

		// khởi tạo danh sách kề
		initGraph();
	}

	// phương thức tiện ích để khởi tạo danh sách kề
	@SuppressWarnings("unchecked")
	private void initGraph() {
		adj = new ArrayList[vertices];
		for (int i = 0; i < vertices; i++) {
			adj[i] = new ArrayList<>();
		}
	}

	// thêm cạnh u-v
	private void addEdge(Integer u, Integer v) {
		adj[u].add(v);
		adj[v].add(u);
	}

	// Hàm này loại bỏ cạnh u-v khỏi đồ thị.
	private void removeEdge(Integer u, Integer v) {
		adj[u].remove(v);
		adj[v].remove(u);
	}

//	 Chức năng chính in Eulerian Trail. Lần đầu tiên nó tìm thấy một mức độ kỳ lạ
//	 đỉnh (nếu có) và sau đó gọi printEulerUtil () để in đường dẫn
	private void printEulerTour() {
		// Tìm một đỉnh có bậc lẻ
		Integer u = 0;
		for (int i = 0; i < vertices; i++) {
			if (adj[i].size() % 2 == 1) {
				u = i;
				break;
			}
		}

		// In chuyến tham quan bắt đầu từ lẻ
		printEulerUtil(u);
		System.out.println();
	}

	// In đường đi Euler bắt đầu từ đỉnh u
	private void printEulerUtil(Integer u) {
		// Định kỳ cho tất cả các đỉnh kề với đỉnh này
		for (int i = 0; i < adj[u].size(); i++) {
			Integer v = adj[u].get(i);
			// Nếu cạnh u-v là cạnh hợp lệ
			if (isValidNextEdge(u, v)) {
				System.out.print(u + "-" + v + " ");

				// Cạnh này đã được sử dụng nên hãy xóa nó ngay bây giờ
				removeEdge(u, v);
				printEulerUtil(v);
			}
		}
	}

	// Chức năng kiểm tra xem cạnh u-v có thể được coi là cạnh tiếp theo trong đường
	// đi Euler
	// hay không
	private boolean isValidNextEdge(Integer u, Integer v) {
//		Cạnh u-v hợp lệ tại một trong các
//		hai trường hợp sau:

		// 1) Nếu v là đỉnh liền kề duy nhất của u tức là kích thước của danh sách đỉnh
		// liền kề là 1
		if (adj[u].size() == 1) {
			return true;
		}

//		2) Nếu có nhiều tính từ, thì
//		u-v không phải là cầu nối Thực hiện các bước sau
//		để kiểm tra xem u-v có phải là cầu nối không
		// 2.a) số lượng đỉnh có thể đạt tới từ u
		boolean[] isVisited = new boolean[this.vertices];
		int count1 = dfsCount(u, isVisited);

		// 2.b) Loại bỏ cạnh (u, v) và sau khi loại bỏ cạnh, đếm các đỉnh có thể đạt
		// được từ u
		removeEdge(u, v);
		isVisited = new boolean[this.vertices];
		int count2 = dfsCount(u, isVisited);

		// 2.c) Thêm cạnh trở lại biểu đồ
		addEdge(u, v);
		return (count1 > count2) ? false : true;
	}

	// Một hàm dựa trên DFS để đếm các đỉnh có thể tới được từ v
	private int dfsCount(Integer v, boolean[] isVisited) {
		// Đánh dấu nút hiện tại là đã truy cập
		isVisited[v] = true;
		int count = 1;
		// Định kỳ cho tất cả các đỉnh kề với đỉnh này
		for (int adj : adj[v]) {
			if (!isVisited[adj]) {
				count = count + dfsCount(adj, isVisited);
			}
		}
		return count;
	}

	public static void main(String a[]) {

		Graph g1 = new Graph(4);
		g1.addEdge(0, 1);
		g1.addEdge(0, 2);
		g1.addEdge(1, 2);
		g1.addEdge(2, 3);
		g1.printEulerTour();

		Graph g2 = new Graph(3);
		g2.addEdge(0, 1);
		g2.addEdge(1, 2);
		g2.addEdge(2, 0);
		g2.printEulerTour();

		Graph g3 = new Graph(5);
		g3.addEdge(1, 0);
		g3.addEdge(0, 2);
		g3.addEdge(2, 1);
		g3.addEdge(0, 3);
		g3.addEdge(3, 4);
		g3.addEdge(3, 2);
		g3.addEdge(3, 1);
		g3.addEdge(2, 4);
		g3.printEulerTour();
	}
}
