package euler;

import java.io.*;
import java.util.*;
import java.util.LinkedList;

//1+2 kiểm tra một đồ thị có phải đồ thị euler và nửa euler (đồ thị vô hướng)
public class UndirectedGraph {
	private int V; // Số đỉnh
	private LinkedList<Integer> adj[];// Danh sách kề

	// Constructor
	public UndirectedGraph(Integer v) {
		V = v;
		adj = new LinkedList[v];
		for (int i = 0; i < v; ++i)
			adj[i] = new LinkedList();
	}

	// Thêm cạnh
	public void addEdge(int v, int w) {
		adj[v].add(w);
		adj[w].add(v);
	}

	public void DFSUtil(int v, boolean visited[]) {
		visited[v] = true;
		Iterator<Integer> i = adj[v].listIterator();
		while (i.hasNext()) {
			int n = i.next();
			if (!visited[n])
				DFSUtil(n, visited);
		}
	}

	// Kiểm tra thành phần liên thông
	public boolean isConnected() {
		// Đánh dấu tất cả các đỉnh là không được thăm
		boolean visited[] = new boolean[V];
		int i;
		for (i = 0; i < V; i++)
			visited[i] = false;

		// Tìm một đỉnh có tung độ khác 0
		for (i = 0; i < V; i++)
			if (adj[i].size() != 0)
				break;

		// Nếu không có cạnh nào trong biểu đồ, trả về true
		if (i == V)
			return true;

		// Bắt đầu duyệt DFS từ một đỉnh có độ khác 0
		DFSUtil(i, visited);

		// Kiểm tra xem tất cả các đỉnh khác 0 có được thăm không
		for (i = 0; i < V; i++)
			if (visited[i] == false && adj[i].size() > 0)
				return false;

		return true;
	}
	// 1+2 kiểm tra một đồ thị có phải đồ thị euler và nửa euler

	/*
	 * Hàm trả về một trong các giá trị sau 0 -> Nếu đồ thị không Eulerian 1 -> Nếu
	 * đồ thị có đường Euler (Bán nguyệt) 2 -> Nếu đồ thị có Đường Euler
	 */
	public int isEulerian() {
		// Kiểm tra xem tất cả các đỉnh khác 0 có được kết nối với nhau không
		if (isConnected() == false)
			return 0;

		// Đếm các đỉnh có bậc lẻ
		int odd = 0;
		for (int i = 0; i < V; i++)
			if (adj[i].size() % 2 != 0)
				odd++;

		// Nếu số lượng nhiều hơn 2, thì biểu đồ không phải là Euler
		if (odd > 2)
			return 0;

		// Nếu số lẻ là 2, thì bán eulerian.
		// Nếu số lẻ là 0, thì eulerian
		// Lưu ý rằng số lẻ không bao giờ có thể là 1 đối với đồ thị vô hướng
		return (odd == 2) ? 1 : 2;
	}

	public void test() {
		int res = isEulerian();
		if (res == 0)
			System.out.println("Đồ thị không là Euler");
		else if (res == 1)
			System.out.println("Đồ thị là nửa Euler");
		else
			System.out.println("Đồ thị là Euler");
	}

	public static void main(String args[]) {

		UndirectedGraph g1 = new UndirectedGraph(5);
		g1.addEdge(1, 0);
		g1.addEdge(0, 2);
		g1.addEdge(2, 1);
		g1.addEdge(0, 3);
		g1.addEdge(3, 4);
		g1.test();

		UndirectedGraph g2 = new UndirectedGraph(5);
		g2.addEdge(1, 0);
		g2.addEdge(0, 2);
		g2.addEdge(2, 1);
		g2.addEdge(0, 3);
		g2.addEdge(3, 4);
		g2.addEdge(4, 0);
		g2.test();

		UndirectedGraph g3 = new UndirectedGraph(5);
		g3.addEdge(1, 0);
		g3.addEdge(0, 2);
		g3.addEdge(2, 1);
		g3.addEdge(0, 3);
		g3.addEdge(3, 4);
		g3.addEdge(1, 3);
		g3.test();

		UndirectedGraph g4 = new UndirectedGraph(3);
		g4.addEdge(0, 1);
		g4.addEdge(1, 2);
		g4.addEdge(2, 0);
		g4.test();

		UndirectedGraph g5 = new UndirectedGraph(3);
		g5.test();
	}
}