package graphMatrix;

import java.util.Stack;

public class UnDirectedGraph extends Graph {

	public UnDirectedGraph(int n) {
		super(n);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addedge(Integer u, Integer v) {
		adjMatrix[u][v] += adjMatrix[u][v];
		adjMatrix[v][u] += adjMatrix[v][u];

	}

	@Override
	public void removeEdge(Integer u, Integer v) {
		adjMatrix[u][v] -= adjMatrix[u][v];
		adjMatrix[v][u] -= adjMatrix[v][u];

	}

	@Override
	public boolean isConnected() {
		// kiem tra tinh lien thong cua do thi ,su dung DFS
		boolean[] visited = new boolean[this.adjMatrix.length]; // mang visited de danh dau cac dinh da duoc tham
		Stack<Integer> stack = new Stack<Integer>();
		Integer v = 0; // v la dinh dang duoc tham(bat dau tham tu dinh 0
		visited[v] = true;
		int count = 0;
		while (!stack.isEmpty()) {
			v = stack.peek();
			boolean found = false;
			// Tim mot dinh j, ke voi v va chua duoc tham
			for (Integer j = 0; j < this.adjMatrix.length; j++)
				if (!visited[j] && this.adjMatrix[v][j] > 0) {
					v = j;
					stack.push(v);
					visited[j] = true;
					found = true;
					count++;
					break;
				}

			if (!found && !stack.isEmpty())
				stack.pop();
		}
		for (int i = 0; i < this.adjMatrix.length; i++)
			if (visited[i] == false)
				return false;
		return true;
	}

	@Override
	public int numOfEdge() {
		int s = 0;
		for (int i = 0; i < adjMatrix.length; i++)
			s += outDegree(i);
		return 0;
	}

	@Override
	public int inDegree(int v) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int outDegree(int v) {
		int res = 0;
		for (int i = 0; i < adjMatrix.length; i++)
			res += adjMatrix[v][i];
		return res;
	}

}
