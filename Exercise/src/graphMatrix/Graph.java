package graphMatrix;

public abstract class Graph {
	// cài đặt đồ thị ma trận kề
	Integer[][] adjMatrix = null;

	public Graph(int n) {
		// n là số đỉnh cho trước
		adjMatrix = new Integer[n][n];

	}

	// them canh
	public abstract void addedge(Integer u, Integer v);

	// xoa canh
	public abstract void removeEdge(Integer u, Integer v);

	// kiem tra su lien thong
	public abstract boolean isConnected();

	// in ma tran
	public void printAdjaencyMatrix() {

	}

	// so canh cua do thi
	public abstract int numOfEdge();

	// nua bac trong
	public abstract int inDegree(int v);

	// nua bac ngoai
	public abstract int outDegree(int v);

}
