package graphicList;

import java.util.ArrayList;
import java.util.List;

public class UndirectedGraph extends Graph {

	@Override
	public void addedge(Integer u, Integer v) {
		if (!adjList.containsKey(u)) {
			if (!adjList.get(u).contains(v))
				adjList.get(u).add(v);
		}
		else {
			List<Integer> values = new ArrayList<Integer>();
			values.add(v);
		}
		if(adjList.containsKey(v)) {
			if (!adjList.get(v).contains(u))
				adjList.get(v).add(u);
		}
		else {
			List<Integer> values = new ArrayList<Integer>();
			values.add(u);
		}

	}

	@Override
	public void removeEdge(Integer u, Integer v) {
		// TODO Auto-generated method stub

	}

	@Override
	public int numEdge() {
		// TODO Auto-generated method stub
		return 0;
	}

}
