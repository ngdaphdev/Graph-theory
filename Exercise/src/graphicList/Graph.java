package graphicList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Graph {
	Map<Integer, List<Integer>> adjList = null;

	public Graph() {
		adjList = new HashMap<Integer, List<Integer>>();

	}

	// them canh
	public abstract void addedge(Integer u, Integer v);
    // xoa canh
	public abstract void removeEdge(Integer u, Integer v);
    //
	public abstract int numEdge();

}
