package ex_graphs1;

import java.util.LinkedList;
import java.util.Queue;

public abstract class Graph {
	protected int A[][] = null;

	public abstract void addEdge(int u, int v);

	public abstract void removeEdge(int u, int v);

	public abstract int numEdge();
	

	public void Duyet_do_thi_su_dung_BFS(int p1) {
		Queue<Integer> queue = new LinkedList<Integer>();
		queue.add(p1);
		boolean[] visited = new boolean[A.length];
		visited[p1] = true;
		int v = 0;
		while (!queue.isEmpty()) {
			v = queue.poll();
			for (int i = 0; i < A.length; i++)
				if (A[v][i] > 0 && !visited[i]) {
					visited[i] = true;
					queue.add(i);
				}
		}

	}
}
