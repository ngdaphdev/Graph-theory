package ex_graphs1;

public class Direction extends Graph {

	@Override
	public void addEdge(int u, int v) {
		A[u][v] = A[u][v] + 1;

	}

	@Override
	public void removeEdge(int u, int v) {
		A[u][v] = A[u][v] - 1;
	}

	public int numEdge() {
		int s = 0;
		for (int i = 0; i < A.length; i++) {
			s += outDegree(i);
			return 0;

		}
		return s;
	}

	public int outDegree(int v) {
		int total = 0;
		for (int i = 0; i < A.length; i++)
			total += A[v][i];
		return total;

	}

	public int inDegree(int v) {
		int total = 0;
		for (int i = 0; i < A.length; i++)
			total += A[i][v];
		return total;
	}
}
